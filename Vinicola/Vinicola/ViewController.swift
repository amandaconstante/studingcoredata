//
//  ViewController.swift
//  Vinicola
//
//  Created by c94292a on 09/12/21.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    var uvasTableView : UITableView = UITableView()
    var identificadorDaCelulaDeUva = "identificadorDaCelulaDeUva"
    var uvas: [Uva] = []
  
    override func viewDidLoad() {
        self.uvasTableView.frame = CGRect(x: 0, y: 60, width: self.view.bounds.width, height: self.view.bounds.height - 60)
        super.viewDidLoad()
        self.view.addSubview(self.uvasTableView)
        self.uvasTableView.dataSource = self
        self.uvasTableView.delegate = self
        self.montaBarraDeNavegacao()
        self.carregaUvasDoBancoDeDados()
    }
    
    func montaBarraDeNavegacao(){
        let navBar = UINavigationBar(frame: CGRect(x: 0, y: 5, width: view.frame.size.width, height: 55))
        view.addSubview(navBar)
        let navItem = UINavigationItem(title: "Uvas 🍇 ")
        let doneItem = UIBarButtonItem(barButtonSystemItem: .add, target: nil, action: #selector(addTapped))
        navItem.rightBarButtonItem = doneItem
        navBar.setItems([navItem], animated: false)
    }
    
    public func carregaUvasDoBancoDeDados(){
        do {
            self.uvas = try DataBaseController.persistentContainer.viewContext.fetch(Uva.fetchRequest())
        } catch {
            print("Nao consegui trazer as informaçoes do banco de dados")
        }
        self.uvasTableView.reloadData()
        
    }
    
    @objc func addTapped(sender: AnyObject) {
        //Criando o primeiro alerta
               let firstAlert = UIAlertController(title: "Adicionar uva", message: "Nome da uva", preferredStyle: .alert)
               //Botao de cancelar o primeiro alerta
               let firstAlertCancelButton = UIAlertAction(title: "Cancelar", style: .default)
               //Botao de ir para o proximo campo do primeiro alerta
               let firstAlertNextButton = UIAlertAction(title: "Próximo", style: .default) { [unowned self] action in
                   
                   //Criando o segundo alerta
                   let secondAlert = UIAlertController(title: "Nova uva",
                                                       message: "Local de Origem",
                                                       preferredStyle: .alert)
                   //Bora de cancelar do segundo alerta
                   let secondAlertCancelButton = UIAlertAction(title: "Cancelar",
                                                               style: .default)
                   
                   //Botao de terminar do segundo alerta
                   let secondAlertFinishButton = UIAlertAction(title: "Terminar", style: .default) { [unowned self] action in
                       
                       if let uvaNome = firstAlert.textFields?.first?.text {
                           if let uvaOrigem = secondAlert.textFields?.first?.text {
                               
                               let context = DataBaseController.persistentContainer.viewContext
                               let uva = Uva(context: context)
                               uva.nome = uvaNome
                               uva.origem = uvaOrigem
                               DataBaseController.saveContext()
                               carregaUvasDoBancoDeDados()
                           }
                       }
                       
                   }
                   
                   secondAlert.addAction(secondAlertCancelButton)
                   secondAlert.addAction(secondAlertFinishButton)
                   secondAlert.addTextField()
                   
                   self.present(secondAlert, animated: true)
                   
               }
               
               firstAlert.addTextField()
               firstAlert.addAction(firstAlertCancelButton)
               firstAlert.addAction(firstAlertNextButton)
               
               self.present(firstAlert, animated: true)
    }

}

extension ViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return uvas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celula = UITableViewCell(style: .subtitle, reuseIdentifier: self.identificadorDaCelulaDeUva)
        celula.textLabel?.text = uvas[indexPath.row].nome
        celula.detailTextLabel?.text = uvas[indexPath.row].origem
        return celula
    }
    
    
}

extension ViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        DataBaseController.persistentContainer.viewContext.delete(self.uvas[indexPath.row])
        DataBaseController.saveContext()
        self.carregaUvasDoBancoDeDados()
        self.uvasTableView.reloadData()
             
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detail = ChangeNameViewController()
        detail.uva = self.uvas[indexPath.row]
        self.showDetailViewController(detail, sender: nil)
    }
}
