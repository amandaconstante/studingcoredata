//
//  ChangeNameViewController.swift
//  Vinicola
//
//  Created by c94292a on 09/12/21.
//

import UIKit
import CoreData

class ChangeNameViewController: UIViewController {

    var uva : Uva?
    @IBOutlet weak var nomeOrigemTextFiled: UITextField!
    @IBOutlet weak var nomeDaUvaTextField: UITextField!
    let context = DataBaseController.persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configuraAtributosDaUvaNaTela()
    }
    
    func configuraAtributosDaUvaNaTela(){
        if let uva = self.uva {
            self.nomeOrigemTextFiled.text = uva.origem
            self.nomeDaUvaTextField.text = uva.nome
        }
    }

    @IBAction func confirmarMudancasESalvarNoBanco(_ sender: Any) {
        if let uva = self.uva {
            guard let uvaNome = uva.nome else {return}
           
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Uva")
            fetchRequest.predicate = NSPredicate(format: "nome == '\(uvaNome)'")

            do {
                let results = try context.fetch(fetchRequest) as? [NSManagedObject]
                if results?.count != 0 {
                    results?[0].setValue(self.nomeDaUvaTextField.text, forKey: "nome")
                    results?[0].setValue(self.nomeOrigemTextFiled.text, forKey: "origem")
                }
            } catch {
                print("Fetch Failed: \(error)")
            }
            DataBaseController.saveContext()
        }
    }
}
